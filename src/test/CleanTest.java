package test;

import java.io.File;

public class CleanTest {

	/**
	 * It cleans all the files that have been download and return the test case to its original state.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String[] ownDirs = new String[10];
		String[] cacheDirs = new String[10];
		for (int i = 0; i < 10; i++) {
			ownDirs[i] = System.getProperty("user.dir") + "/p" + i;
			cacheDirs[i] = System.getProperty("user.dir") + "/p" + i + "c";
		}

		for (String ownDir : ownDirs) {
			deleteMetaDir(ownDir);
		}
		for (String cacheDir : cacheDirs) {
			deleteMetaDir(cacheDir);
			deleteFiles(cacheDir);
		}
		System.out.println("Finished cleaning test.");
	}

	private static void deleteMetaDir(String parent) {
		File metaDir = new File(parent + "/.meta");
		if (metaDir.isDirectory() && metaDir.exists()) {
			deleteFiles(parent + "/.meta");
			metaDir.delete();
		}
	}

	private static void deleteFiles(String parent) {
		File dir = new File(parent);
		if (dir.list() != null) {
			for (File file : dir.listFiles()) {
				if (file.isFile() && file.exists()) {
					file.delete();
				}
			}
		}
	}

}
