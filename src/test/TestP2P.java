package test;

import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

import peer.*;
import util.Util;

/**
 * This class is the main class to start the test. 
 * Note that to clean test before started.
 *
 */
public class TestP2P {

	private static Peer[] allPeers;
	private static String[][] allFileNames;

	public static void main(String[] args) throws Exception {
		initAllFileNames();
		startTest();
	}

	private static void initAllFileNames() {
		allFileNames = new String[10][10];
		for (int peerId = 0; peerId < 10; peerId++) {
			allFileNames[peerId][0] = "a" + peerId;
			allFileNames[peerId][1] = "b" + peerId;
			allFileNames[peerId][2] = "c" + peerId;
			allFileNames[peerId][3] = "d" + peerId;
			allFileNames[peerId][4] = "e" + peerId;
			allFileNames[peerId][5] = "f" + peerId;
			allFileNames[peerId][6] = "g" + peerId;
			allFileNames[peerId][7] = "h" + peerId;
			allFileNames[peerId][8] = "i" + peerId;
			allFileNames[peerId][9] = "pic" + peerId + ".jpg";
		}
	}

	private static void startTest() throws Exception {
		startAllPeers(); // peer 0, 1, 2, 3, 4 will do downloading
		Peer[] evalPeers = new Peer[5];
		for (int i = 0; i < 5; i++) {
			evalPeers[i] = allPeers[i];
		}
		Peer[] simPeers = new Peer[5]; // peer 5, 6, 7, 8, 9 will simulate files modification
		for (int i = 5; i < 10; i++) {
			simPeers[i - 5] = allPeers[i];
		}

		initTest(simPeers); // let peer 5, 6, 7, 8, 9 have each other's files
		scheduleModifyFiles(simPeers);
		Thread.sleep(5000);

		// collect measurements from peer 5, 6, 7, 8, 9 every 5s
		Monitor.collectMeasurements(simPeers, 5);

		// peer 0, 1, 2, 3, 4 download from peer 5, 6, 7, 8, 9
		for (Peer evalPeer : evalPeers) {
			for (Peer simPeer : simPeers) {
				for (String fileName : allFileNames[simPeer.getId()]) {
					evalPeer.spawnDownloadThread(fileName);
				}
			}
		}

	}

	private static void startAllPeers() throws Exception {
		allPeers = new Peer[10];
		for (int id = 0; id < 10; id++) {
			allPeers[id] = new Peer(id);
			allPeers[id].startService();
		}
	}

	private static void initTest(Peer[] peers) {
		// for every peer in the peer list, download every other peer's every file
		for (Peer peer : peers) {
			for (Peer fromPeer : peers) {
				if (peer.getId() != fromPeer.getId()) {
					for (String fileName : allFileNames[fromPeer.getId()]) {
						peer.spawnDownloadThread(fileName);
					}
				}
			}
		}
	}

	private static void scheduleModifyFiles(Peer[] peers) {
		Timer t = new Timer();
		t.scheduleAtFixedRate(new ModifyFilesTask(peers), 3000, 3000); // modify files every 3s
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t.cancel(); // stop simulating files modification after 30s
	}

	private static class ModifyFilesTask extends TimerTask {
		private Peer[] peers;

		ModifyFilesTask(Peer[] peers) {
			this.peers = peers;
		}

		@Override
		public void run() {
			// for each peer in the list, modify every owned file of it
			for (Peer peer : this.peers) {
				for (String fileName : allFileNames[peer.getId()]) {
					try {
						modifyFile(fileName, peer.getId());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private static void modifyFile(String filename, int peerId) throws IOException {
		String path = System.getProperty("user.dir") + "/p" + peerId + "/" + filename;
		File f = new File(path);
		byte[] content = Util.fileToBytes(f);
		byte b = content[0];
		content[0] = (byte) ~b; // modify first byte
		Util.bytesToFile(f, content);
	}

}
