package test;

import java.util.Timer;
import java.util.TimerTask;

import peer.Peer;
import peer.Statistics;

/**
 * This class is to monitor the statistics for each peer for performance evaluation.
 *
 */
public class Monitor {

	/**
	 * Start a fixed rate scheduler to run the collect task
	 * 
	 * @param peers
	 * @param sec
	 */
	public static void collectMeasurements(Peer[] peers, int sec) {
		Timer t = new Timer();
		t.scheduleAtFixedRate(new MeasurementsCollector(peers), 0, sec * 1000);
	}

	/**
	 * A timer task for collecting the statistics information of the peer
	 * 
	 */
	protected static class MeasurementsCollector extends TimerTask {
		Peer[] peers;

		protected MeasurementsCollector(Peer[] peers) {
			super();
			this.peers = peers;
		}

		@Override
		public void run() {
			statisticsOfConsisEfficiency();
		}

		protected void statisticsOfConsisEfficiency() {
			int allPeersQueryCount = 0;
			int allPeersBadQueryCount = 0;

			System.out.println();
			System.out.printf("%-6s", "Peer");
			System.out.printf("%-9s", "Queries");
			System.out.printf("%-13s", "Bad Queries");
			System.out.printf("%-12s", "Efficiency");
			System.out.println("\n----  -------  -----------  ----------");
			for (Peer p : peers) {
				int queryCount = p.getStatistics().getQueryCount();
				int badQueryCount = p.getStatistics().getBadQueryCount();
				System.out.printf("%-6d", p.getId());
				System.out.printf("%-9d", queryCount);
				System.out.printf("%-13d", badQueryCount);
				System.out.printf("%9.2f", queryCount == 0 ? 0.0 : (float) (queryCount - badQueryCount) / queryCount * 100);
				System.out.println("%");
				allPeersQueryCount += queryCount;
				allPeersBadQueryCount += badQueryCount;
			}
			System.out.println("----  -------  -----------  ----------");
			System.out.printf("%-6s", "Avg");
			System.out.printf("%-9.1f", (float) allPeersQueryCount / peers.length);
			System.out.printf("%-13.1f", (float) allPeersBadQueryCount / peers.length);
			System.out.printf("%9.2f", allPeersQueryCount == 0 ? 0.0 : (float) (allPeersQueryCount - allPeersBadQueryCount) / allPeersQueryCount * 100);
			System.out.println("%");
		}

		protected void statisticsOfRespTime() {
			int allPeersMsgCount = 0;
			double allPeersRespTime = 0;

			System.out.println();
			System.out.printf("%-6s", "Peer");
			System.out.printf("%-11s", "Msg Count");
			System.out.printf("%-21s", "Total Resp Time (s)");
			System.out.printf("%-20s", "Avg Resp Time (s)");
			System.out.println("\n----  ---------  -------------------  -------------------");
			for (Peer p : peers) {
				double sumRespTime = p.getStatistics().getSumRespTime() / Statistics.NANO_FACTOR;
				int validMsgCount = p.getStatistics().getValidMsgCount();
				System.out.printf("%-6d", p.getId());
				System.out.printf("%-11d", validMsgCount);
				System.out.printf("%-21.3f", sumRespTime);
				System.out.printf("%-20.3f", sumRespTime / validMsgCount);
				System.out.println();
				allPeersMsgCount += validMsgCount;
				allPeersRespTime += sumRespTime;
			}
			System.out.println("----  ---------  -------------------  -------------------");
			System.out.printf("%-6s", "Avg");
			System.out.printf("%-11d", allPeersMsgCount / peers.length);
			System.out.printf("%-21.3f", allPeersRespTime / peers.length);
			System.out.printf("%-20.3f", allPeersRespTime / allPeersMsgCount);
			System.out.println();
		}
	}
}
