package model;

import java.io.Serializable;
import java.util.Stack;

import peer.Peer;
import model.Type;

/**
 * This class is the data structure to store message used by peers to communicate
 * and it impelments Serializable interface for TCP transmission
 *
 */
public class Message implements Serializable, Comparable<Message> {

	/**
	 * Message is a Data Structure to store every necessary information in transmission
	 */
	private static final long serialVersionUID = -7540429271538987887L;
	private int[] id;// message's id = peerId + sequenceNumber
	private Type.MessageType type;
	private int TTL;
	private Stack<Integer> upstreamPeer;
	private FileMeta meta;
	private String[] ipPortId;// ip, port, id of the sender
	private long timeStamp;

	private byte[] fileContent;

	public Message(int peerId, int sequenceNumber, int TTL, FileMeta meta, Type.MessageType type) {
		this.id = new int[2];
		this.id[0] = peerId;
		this.id[1] = sequenceNumber;
		this.TTL = TTL;
		this.upstreamPeer = new Stack<Integer>();
		this.meta = meta;
		this.type = type;
	}
	
	public Message(Peer peer, FileMeta meta, Type.MessageType type) {
		this(peer.getId(), peer.generateSN(), peer.getConfig().getTTL(), meta, type);
		setSenderIpPortId(peer);
	}

	/**
	 * Get the ID of this message instance
	 * 
	 * @return id Message ID = Peer ID + Sequence Number
	 */
	public int[] getId() {
		return this.id;
	}

	public int getTTL() {
		return this.TTL;
	}

	public FileMeta getFileMeta() {
		return this.meta;
	}

	public Type.MessageType getType() {
		return this.type;
	}

	public void setType(Type.MessageType type) {
		this.type = type;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = new byte[fileContent.length];
		System.arraycopy(fileContent, 0, this.fileContent, 0, fileContent.length);
	}

	/**
	 * Get the IP, port ID of the peer who sends the message
	 * 
	 * @return ipPortId String[0]=ip, String[1]=port, String[2]=id
	 */
	public String[] getSenderIpPortId() {
		return this.ipPortId;
	}

	/**
	 * Set the IP, port, ID of the peer who sends the message
	 * 
	 * @param ip
	 * @param port
	 * @param id
	 */
	public void setSenderIpPortId(String ip, int port, int id) {
		String[] ipPortId = new String[3];
		ipPortId[0] = ip;
		ipPortId[1] = String.valueOf(port);
		ipPortId[2] = String.valueOf(id);
		this.ipPortId = ipPortId;
	}
	
	public void setSenderIpPortId(Peer peer) {
		setSenderIpPortId(peer.getIp(), peer.getPort(), peer.getId());
	}

	public void minusTTL() {
		this.TTL -= 1;
	}

	/**
	 * Get the ID of peer at the top of the stack
	 * 
	 * @return int The ID of peer in this message upstreamPeer stack
	 */
	public int popPeer() {
		return upstreamPeer.pop();
	}

	/**
	 * Push the ID of peer to the top of the stack
	 * 
	 * @param peerId
	 */
	public void pushPeer(int peerId) {
		this.upstreamPeer.push(peerId);
	}

	/**
	 * 
	 * @return boolean if the upStreamPeer stack has element
	 */
	public boolean hasUpstreamPeer() {
		return !upstreamPeer.isEmpty();
	}

	public String toString() {
		String info = "message{";
		info += "id=" + this.id[0] + this.id[1] + ", ";
		info += "type=" + this.type + ", ";
		info += "ttl=" + this.TTL + ", ";
		info += "file=\"" + this.meta.getName() + "\", ";
		info += "upstream=";
		for (int i : this.upstreamPeer) {
			info += "Peer" + i + "|";
		}
		info += "}";
		return info;
	}

	@Override
	public int compareTo(Message m) {
		if (this.timeStamp < m.getTimeStamp())
			return -1;
		else if (this.timeStamp == m.getTimeStamp())
			return 0;
		else
			return 1;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Message)) {
			return false;
		}

		Message m = (Message) o;
		return (this.id[0] == m.getId()[0] && this.id[1] == m.getId()[1] && this.type.equals(m.type));
	}

	@Override
	public int hashCode() {
		return this.id[0] + this.id[1] + this.type.ordinal();
	}
}
