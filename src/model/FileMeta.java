package model;

import java.io.Serializable;
/**
 * This class stores the necessary metadata for a file 
 * and implements the Serializable interface for TCP transmission.
 *
 */
public class FileMeta implements Serializable {

	private static final long serialVersionUID = -5973445379253865421L;

	private String name;
	private int owner; // used by both push and pull mode
	private Type.FileType state; // used by both push and pull mode
	private int version; // used by both push and pull mode
	private int TTR; // used by pull mode
	private long lastModified; // used by both push and pull mode

	public FileMeta(String name, int owner, Type.FileType state, int version, int TTR, long lastModified) {
		this.name = name;
		this.owner = owner;
		this.state = state;
		this.version = version;
		this.TTR = TTR;
		this.lastModified = lastModified;
	}

	public String getName() {
		return name;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public Type.FileType getState() {
		return state;
	}

	public void setState(Type.FileType state) {
		this.state = state;
	}

	public int getTTR() {
		return TTR;
	}

	public void setTTR(int ttr) {
		this.TTR = ttr;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void increaseVersion() {
		this.version++;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public String toString() {
		String info = "meta{";
		info += "file=" + "\"" + this.name + "\", ";
		info += "owner=" + this.owner + ", ";
		info += "state=" + this.state + ", ";
		info += "TTR=" + this.TTR + ", ";
		info += "ver=" + this.version + ", ";
		info += "lastMod=" + this.lastModified + "}";
		return info;
	}

}
