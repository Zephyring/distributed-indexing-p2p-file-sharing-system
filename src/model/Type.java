package model;

/**
 * This class defines all the type information
 * such as Message type and file state type.
 *
 */
public class Type {

	public static enum MessageType {
		Query, HitQuery, DownloadRequest, DownloadReply, Invalidate, TTRRequest, TTRReply, CompareLastMod /* performance evaluation */
	};

	// push-based method only have Valid/Invalid files
	// pull-based method have Valid/Invalid/TTR_Expired files
	public static enum FileType {
		Valid, Invalid, TTR_Expired;

		public static FileType parseState(String state) {
			if (Valid.toString().equals(state))
				return Valid;
			else if (TTR_Expired.toString().equals(state))
				return TTR_Expired;
			else
				return Invalid;
		}
	};

}
