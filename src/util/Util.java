package util;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import model.FileMeta;
import model.Type;

/**
 * This class handles all the I/O operations 
 * to read/write content in disk to/from memory.
 *
 */
public class Util {
	/**
	 * Read property value from the local configuration file by key
	 * 
	 * @param key
	 *            the key for your query
	 * @return value the result you look up
	 */
	public static String readProperty(String configFile, String key) {
		Properties p = readAllProperties(configFile);
		String value = p.getProperty(key);
		return value;
	}

	public static Properties readAllProperties(String configFile) {
		Properties p = new Properties();
		String filePath = System.getProperty("user.dir") + "/" + configFile;
		InputStream in = null;
		try {
			in = new BufferedInputStream(new FileInputStream(filePath));
			p.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return p;
	}

	/**
	 * Read all the neighbor IDs based on the peer ID and what topology we use
	 * 
	 * @param id
	 * @param topology
	 * @return
	 */
	public static String readNeighbor(int id, String topology) {
		String filePath = System.getProperty("user.dir") + "/" + topology;
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
			while ((line = br.readLine()) != null) {
				if (line.startsWith("id")) {
					if (line.split("=")[1].equals(String.valueOf(id))) {
						br.readLine();
						br.readLine();
						line = br.readLine();
						break;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Reading neighbor errors");
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		return line != null ? line.split("=")[1].trim() : null;
	}

	/**
	 * Read the IP and Port of the peer by Peer Id and topology
	 * 
	 * @param id
	 * @param topology
	 * @return
	 */
	public static String[] readAddr(int id, String topology) {
		String[] addr = null;
		String filePath = System.getProperty("user.dir") + "/" + topology;
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
			while ((line = br.readLine()) != null) {
				if (line.startsWith("id")) {
					if (line.split("=")[1].equals(String.valueOf(id))) {
						String ipLine = br.readLine();
						String basePortLine = br.readLine();
						addr = new String[2];
						addr[0] = ipLine.split("=")[1].trim();
						String basePort = basePortLine.split("=")[1].trim();
						addr[1] = String.valueOf(Integer.parseInt(basePort) + id); // port = basePort + peerId
						break;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Reading address errors");
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return addr;
	}

	/**
	 * Read the IP and Port of all peers by topology
	 * 
	 * @param topology
	 * @return
	 */
	public static Map<Integer, String[]> readAllPeersAddrs(String topology) {
		Map<Integer, String[]> allPeersAddrs = new HashMap<Integer, String[]>();
		String filePath = System.getProperty("user.dir") + "/" + topology;
		BufferedReader br = null;
		String line = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
			while ((line = br.readLine()) != null) {
				if (line.startsWith("id")) {
					int id = Integer.parseInt(line.split("=")[1].trim());
					String ipLine = br.readLine();
					String basePortLine = br.readLine();
					String[] addr = new String[2];
					addr[0] = ipLine.split("=")[1].trim();
					String basePort = basePortLine.split("=")[1].trim();
					addr[1] = String.valueOf(Integer.parseInt(basePort) + id); // port = basePort + peerId
					allPeersAddrs.put(id, addr);
				}
			}
		} catch (Exception e) {
			System.out.println("Reading all peers addresses errors");
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return allPeersAddrs;
	}

	/**
	 * Read the local file list in the shared directory
	 * 
	 * @param peerId
	 * @return
	 * @throws
	 */
	public static HashMap<String, File> readLocalFiles(int peerId, String dirSuffix) {
		String path = System.getProperty("user.dir") + "/p" + String.valueOf(peerId) + dirSuffix;
		File directory = new File(path);
		if (directory.list() != null) {
			HashMap<String, File> files = new HashMap<String, File>();
			for (String fileName : directory.list()) {
				File file = Util.getFile(fileName, peerId, dirSuffix);
				if (file.exists() && file.isFile()) {
					files.put(fileName, file);
				}
			}
			return files;
		} else {
			System.out.println("No local directory prepared.");
			System.out.println("Peer " + peerId + " Exit");
			System.exit(0);
		}
		return null;
	}

	/**
	 * Get the file instance based on the filename and the peer ID
	 * 
	 * @param filename
	 * @param peerId
	 * @return
	 * @throws IOException
	 */
	public static File getFile(String filename, int peerId, String dirSuffix) {
		String path = System.getProperty("user.dir") + "/p" + String.valueOf(peerId) + dirSuffix + "/" + filename;
		File f = new File(path);
		return f;
	}

	/**
	 * Change file content to byte array Used for transmitting file
	 * 
	 * @param f
	 * @return
	 * @throws IOException
	 */
	public static byte[] fileToBytes(File f) throws IOException {
		if (!f.exists()) {
			throw new FileNotFoundException();
		}

		ByteArrayOutputStream bos = new ByteArrayOutputStream((int) f.length());
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
		byte[] buffer = new byte[1024];
		int len = 0;
		while (-1 != (len = in.read(buffer))) {
			bos.write(buffer, 0, len);
		}

		byte[] bytes = new byte[bos.size()];
		System.arraycopy(bos.toByteArray(), 0, bytes, 0, bos.size());
		in.close();
		bos.close();

		return bytes;
	}

	/**
	 * Change byte array to file content Used for downloading file
	 * 
	 * @param filePath
	 * @param fileContent
	 * @throws IOException
	 */
	public static void bytesToFile(File f, byte[] fileContent) throws IOException {
		if (!f.exists()) {
			f.createNewFile();
		}
		RandomAccessFile raf = new RandomAccessFile(f, "rw");
		raf.write(fileContent);
		raf.close();
	}

	public static boolean flushAllFileMetaToDisk(Map<String, FileMeta> mem, int peerId, String dirSuffix) {
		boolean success = true;
		for (Map.Entry<String, FileMeta> e : mem.entrySet()) {
			FileMeta meta = e.getValue();
			success = success && Util.flushFileMetaToDisk(meta, peerId, dirSuffix);
		}

		return success;
	}

	public static boolean flushFileMetaToDisk(FileMeta meta, int peerId, String dirSuffix) {
		String errMsg = "Peer " + peerId + " failed to flush " + meta + " to disk.";
		Util.deleteMetaFile(meta.getName(), peerId, dirSuffix);
		File metaFile = null;
		try {
			metaFile = getMetaFile(meta.getName(), peerId, dirSuffix);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println(errMsg);
			return false;
		}

		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(metaFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println(errMsg);
			return false;
		}

		Properties properties = new Properties();
		properties.setProperty("name", meta.getName());
		properties.setProperty("owner", String.valueOf(meta.getOwner()));
		properties.setProperty("state", meta.getState().toString());
		properties.setProperty("TTR", String.valueOf(meta.getTTR()));
		properties.setProperty("version", String.valueOf(meta.getVersion()));
		properties.setProperty("lastModified", String.valueOf(meta.getLastModified()));
		boolean success = true;
		try {
			properties.storeToXML(outputStream, "");
		} catch (IOException e) {
			e.printStackTrace();
			success = false;
			System.err.println(errMsg);
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// if (success) {
		// System.out.println("Peer " + peerId + " flushes " + meta + " to disk successfully.");
		// }
		return success;
	}

	private static File getMetaFile(String fileName, int peerId, String dirSuffix) throws IOException {
		String path = System.getProperty("user.dir") + "/p" + String.valueOf(peerId) + dirSuffix + "/.meta/" + fileName + ".xml";
		File file = new File(path);
		File parent = file.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		if (!file.exists()) {
			file.createNewFile();
		}

		return file;
	}

	public static Map<String, FileMeta> readLocalAllMetaFiles(int peerId, String dirSuffix) {
		String path = System.getProperty("user.dir") + "/p" + String.valueOf(peerId) + dirSuffix + "/.meta";
		File directory = new File(path);
		Map<String, FileMeta> mem = new ConcurrentHashMap<String, FileMeta>();
		if (directory.list() != null) {
			for (String metaFileName : directory.list()) {
				FileMeta meta = Util.readLocalMetaFile(metaFileName, peerId, dirSuffix);
				if (meta != null) {
					mem.put(meta.getName(), meta);
				}
			}
		}
		return mem;
	}

	public static FileMeta readLocalMetaFile(String metaFileName, int peerId, String dirSuffix) {
		String path = System.getProperty("user.dir") + "/p" + String.valueOf(peerId) + dirSuffix + "/.meta/" + metaFileName;
		File file = new File(path);
		if (file.isDirectory() || !file.getName().endsWith(".xml")) {
			return null;
		}
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(file);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		Properties properties = new Properties();
		boolean success = true;
		try {
			properties.loadFromXML(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		FileMeta meta = null;
		if (success) {
			String name = properties.getProperty("name");
			int owner = Integer.parseInt(properties.getProperty("owner"));
			Type.FileType state = Type.FileType.parseState(properties.getProperty("state"));
			int TTR = Integer.parseInt(properties.getProperty("TTR"));
			int version = Integer.parseInt(properties.getProperty("version"));
			long lastModified = Long.parseLong(properties.getProperty("lastModified"));
			meta = new FileMeta(name, owner, state, version, TTR, lastModified);
			// System.out.println("Peer " + peerId + " reads " + meta + " from disk.");
		}
		return meta;
	}

	public static void deleteMetaFile(String fileName, int peerId, String dirSuffix) {
		String path = System.getProperty("user.dir") + "/p" + String.valueOf(peerId) + dirSuffix + "/.meta/" + fileName + ".xml";
		File metaFile = new File(path);
		if (metaFile.exists()) {
			metaFile.delete();
			// System.out.println("Peer " + peerId + " deleted \"" + metaFile.getName() + "\".");
		}
	}

	public static void deleteFile(String fileName, int peerId, String dirSuffix) {
		String path = System.getProperty("user.dir") + "/p" + String.valueOf(peerId) + dirSuffix + "/" + fileName;
		File file = new File(path);
		if (file.exists()) {
			file.delete();
			System.out.println("Peer " + peerId + " deleted \"" + file.getName() + "\" due to lack of meta information.");
		}
	}
}
