package transmission;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

import peer.Peer;
import model.Message;

/**
 * This class is the TCP server proxy.
 *
 */
public class TCPServer {

	private ServerSocket serverSocket;
	private Peer peer;

	public TCPServer(Peer p) {
		this.peer = p;
	}

	/**
	 * Start the server socket with specific port
	 */
	public void start() {
		try {
			serverSocket = new ServerSocket(this.peer.getPort());
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.listen();
	}

	/**
	 * Start listenning for any incoming client connection.
	 * Once new connection comes, start a new thread to handle the connection
	 */
	private void listen() {
		while (true) {
			try {
				Socket client = serverSocket.accept();
				new Thread(new ProcessMessage(this.peer, client)).start();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	/**
	 * A runnable class to hand message over to peer for processing.
	 *
	 */
	protected class ProcessMessage implements Runnable {
		private Message m;
		private Peer peer;
		private Socket client;

		protected ProcessMessage(Peer p, Socket client) {
			this.peer = p;
			this.client = client;
		}

		@Override
		public void run() {
			try {
				// if input stream is a message, read object
				ObjectInputStream objInput = new ObjectInputStream(client.getInputStream());
				Object o = objInput.readObject();
				if (o instanceof Message) {
					m = (Message) o;
					peer.processMessage(m);
				}

				client.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
