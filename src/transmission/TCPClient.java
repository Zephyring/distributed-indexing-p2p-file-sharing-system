package transmission;

import java.io.ObjectOutputStream;
import java.net.Socket;

import model.Message;

/**
 * This class is the TCP client proxy.
 *
 */
public class TCPClient {

	private String ip;
	private int port;
	private Socket socket;

	public TCPClient(String ip, int port) {
		this.ip = ip;
		this.port = port;
		this.bindSocket();
	}
	
	/**
	 * Bind the socket to the specific ip and port
	 */
	private void bindSocket() {
		try {
			socket = new Socket(ip, port);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("TCPClient binding errors.");
		}
	}

	/**
	 * Send message out as an object into the output stream, serialization is automatic.
	 * @param m
	 */
	public void sendMessage(Message m) {
		try {
			ObjectOutputStream objOut = new ObjectOutputStream(socket.getOutputStream());
			objOut.writeObject(m);
			objOut.close();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("TCPClient sending messages errors.");
		}
	}

}
