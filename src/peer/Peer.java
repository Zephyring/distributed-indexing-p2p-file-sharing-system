package peer;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import model.*;
import transmission.*;
import util.*;

/**
 * This class is the implementation of Peer.
 * And all the message processing logic are implemented here.
 *
 */
public class Peer {
	Configuration config = new Configuration();

	private int id;
	private String ip;
	private int port;
	private TCPServer server;

	private Map<String, FileMeta> ownFiles;
	private Map<String, FileMeta> cachedFiles;

	private Set<Integer> neighbors;
	private Map<Integer, String[]> peersAddrs;

	private Queue<Message> oldMessages; // record already seen messages to prevent from re-processing it again
	private int msgSequenceNumber;

	Scheduler scheduler;

	Statistics statistics;

	public Peer(int id) throws Exception {
		this.id = id;
		this.ip = this.config.getPeerIp();
		this.port = this.config.getBasePort() + this.id;

		this.initFiles();

		this.initNeighbor();
		this.peersAddrs = Util.readAllPeersAddrs(this.config.getTopology());

		this.oldMessages = new PriorityQueue<Message>();
		this.msgSequenceNumber = 0;

		this.scheduler = new Scheduler(this);
		this.statistics = new Statistics();
	}

	public Configuration getConfig() {
		return this.config;
	}

	public int getId() {
		return this.id;
	}

	public String getIp() {
		return this.ip;
	}

	public int getPort() {
		return this.port;
	}

	public int generateSN() {
		return this.msgSequenceNumber++;
	}

	public Map<String, FileMeta> getOwnFiles() {
		return this.ownFiles;
	}

	public Map<String, FileMeta> getCachedFiles() {
		return this.cachedFiles;
	}

	public Map<Integer, String[]> getPeersAddrs() {
		return this.peersAddrs;
	}

	public Statistics getStatistics() {
		return this.statistics;
	}

	/**
	 * read owned files and cached files from disk to memory
	 */
	private void initFiles() {
		this.ownFiles = new ConcurrentHashMap<String, FileMeta>();
		this.cachedFiles = new ConcurrentHashMap<String, FileMeta>();
		this.ownFiles = Util.readLocalAllMetaFiles(this.id, ""); // init owned files meta-info from disk
		this.cachedFiles = Util.readLocalAllMetaFiles(this.id, "c"); // init cached files meta-info from disk
	}

	/**
	 * synchronize owned files and cached files between disk and memory
	 */
	protected void syncFiles() {
		this.syncOwnFiles();
		this.syncCachedFiles();
	}

	/**
	 * synchronize owned files between disk and memory
	 */
	protected void syncOwnFiles() {
		// get current disk-stored files
		HashMap<String, File> filesOnDisk = Util.readLocalFiles(this.id, "");

		// if file exists on disk but no meta record in memory
		for (Map.Entry<String, File> diskFile : filesOnDisk.entrySet()) {
			String fileName = diskFile.getKey();
			if (!this.ownFiles.containsKey(fileName)) {
				long lastModified = diskFile.getValue().lastModified();
				FileMeta fm = new FileMeta(fileName, this.id, Type.FileType.Valid, 0, this.config.getTTR(), lastModified);
				this.ownFiles.put(fileName, fm); // add newly added file meta to memory
				Util.flushFileMetaToDisk(fm, this.id, ""); // write newly added file meta to disk
			}
		}

		// if file record exists in memory but the file not on disk or modified on disk
		for (Map.Entry<String, FileMeta> memFile : this.ownFiles.entrySet()) {
			String fileName = memFile.getKey();
			FileMeta fm = memFile.getValue();
			if (!filesOnDisk.containsKey(fileName)) {
				this.ownFiles.remove(fileName); // remove the file meta from memory
				Util.deleteMetaFile(fileName, this.id, ""); // delete the file meta from disk

				// if push-based consistency, send "delete" message to its copies
				if (this.config.getConsistencyMethod().equalsIgnoreCase("push")) {
					fm.setVersion(-1);
					fm.setLastModified(-1);
					FileMeta msgMeta = new FileMeta(fm.getName(), fm.getOwner(), fm.getState(), fm.getVersion(), fm.getTTR(), fm.getLastModified());
					Message m = new Message(this, msgMeta, Type.MessageType.Invalidate);
					this.recordMessage(m);
					this.relayForwardMessage(m);
				}
			} else {
				File diskFile = filesOnDisk.get(fileName);
				if (fm.getLastModified() != diskFile.lastModified()) {
					fm.setLastModified(diskFile.lastModified());
					fm.increaseVersion();
					Util.flushFileMetaToDisk(fm, this.id, ""); // write change of the file meta to disk

					// if push-based consistency, send "Invalidate" message to its copies
					if (this.config.getConsistencyMethod().equalsIgnoreCase("push")) {
						FileMeta msgMeta = new FileMeta(fm.getName(), fm.getOwner(), fm.getState(), fm.getVersion(), fm.getTTR(), fm.getLastModified());
						Message m = new Message(this, msgMeta, Type.MessageType.Invalidate);
						this.recordMessage(m);
						this.relayForwardMessage(m);
					}
				}
			}
		}

	}

	/**
	 * synchronize cached files between disk and memory
	 */
	protected void syncCachedFiles() {
		// get current disk-stored files
		HashMap<String, File> filesOnDisk = Util.readLocalFiles(this.id, "c");

		// if file exists on disk but no meta record in memory
		for (Map.Entry<String, File> diskFile : filesOnDisk.entrySet()) {
			String fileName = diskFile.getKey();
			if (!this.cachedFiles.containsKey(fileName)) {
				Util.deleteFile(fileName, this.id, "c"); // delete the file copy due to lack of meta-info
			}
		}

		// if file record exists in memory but the file not on disk
		for (Map.Entry<String, FileMeta> memFile : this.cachedFiles.entrySet()) {
			String fileName = memFile.getKey();
			FileMeta fm = memFile.getValue();
			if (!filesOnDisk.containsKey(fileName)) {
				// set state to "Invalid"
				// (will be download next time the DownloadNewCopies schedule executed)
				fm.setState(Type.FileType.Invalid);
				Util.flushFileMetaToDisk(fm, this.id, "c");
			}
		}
	}

	/**
	 * Read neighbor ID information from topology configuration file
	 */
	private void initNeighbor() {
		String neighbors = Util.readNeighbor(this.id, this.config.getTopology());
		this.neighbors = new HashSet<Integer>();
		for (String n : neighbors.split(" ")) {
			int peerId = Integer.parseInt(n.trim());
			this.neighbors.add(peerId);
		}
	}

	/**
	 * Start the server side of the peer, and all the schedulers
	 */
	public void startService() {
		StartServer ss = new StartServer(this);
		Thread t = new Thread(ss);
		t.start();
		// synchronize files information with local disk every n seconds
		this.scheduler.scheduleSyncFiles(0, Configuration.FILES_SYNC_PERIOD);

		// if pull-based, check file copies in the local cache folder every n seconds. If TTR expired, send TTR request
		if (this.config.getConsistencyMethod().equalsIgnoreCase("pull")) {
			for (Map.Entry<String, FileMeta> e : this.cachedFiles.entrySet()) {
				String fileName = e.getKey();
				this.scheduler.scheduleSetTTRExpired(this, fileName);
			}
			this.scheduler.scheduleTTRExpiredCheck(4, this.config.getPullPeriod());
		}

		// check file copies in the local cache folder every n seconds. If invalid, download new copies
		this.scheduler.scheduleDownloadNewCopies(8, this.config.getRefreshPeriod());

	}

	/**
	 * Check metadata of all cached files in memory
	 * and determines to download a new one if it is invalid
	 * based on pull or push. 
	 */
	protected void downloadNewCopies() {
		for (Map.Entry<String, FileMeta> cachedFile : this.cachedFiles.entrySet()) {
			FileMeta meta = cachedFile.getValue();
			// System.out.println("[REFRESH] Checking if " + meta + " has non-Valid state.");

			boolean needRenew = false;
			if (this.getConfig().getConsistencyMethod().equals("pull")) {
				if (meta.getState() == Type.FileType.Invalid) {
					needRenew = true;
				}
			} else {
				if (meta.getState() != Type.FileType.Valid) {
					needRenew = true;
				}
			}

			if (needRenew) {
				System.out.println("[REFRESH] Peer " + this.id + ": " + meta + " not in Valid state.");
				// send download request to owner peer
				FileMeta msgMeta = new FileMeta(meta.getName(), meta.getOwner(), meta.getState(), meta.getVersion(), meta.getTTR(), meta.getLastModified());
				Message m = new Message(this, msgMeta, Type.MessageType.DownloadRequest);
				String[] destIpPort = this.peersAddrs.get(meta.getOwner());
				this.sendMessage(m, destIpPort[0], Integer.parseInt(destIpPort[1]));
				System.out.println("[REFRESH] Peer " + this.id + " sends dowanload request to Peer " + meta.getOwner() + " to renew \"" + meta.getName() + "\".");
			}
		}
	}

	/**
	 * Set the metadata of file state to be expired given a file name
	 * @param fileName
	 */
	protected void setTTRExpired(String fileName) {
		FileMeta meta = this.cachedFiles.get(fileName);
		meta.setState(Type.FileType.TTR_Expired);
		Util.flushFileMetaToDisk(meta, this.id, "c");
		System.out.println("[TIME-TO-REFRESH] Peer " + this.id + ": TTR of \"" + meta.getName() + "\" is expired.");
	}

	/**
	 * Check metadata of all cached file in memory
	 * and send TTR request to owner peer to notify the TTR refreshment.
	 */
	protected void checkTTRExpired() {
		for (Map.Entry<String, FileMeta> cachedFile : this.cachedFiles.entrySet()) {
			FileMeta meta = cachedFile.getValue();
			// System.out.println("[LAZY PULL] Checking if " + meta + " has TTR Expired state.");

			if (meta.getState() == Type.FileType.TTR_Expired) {
				// send TTR request to owner peer
				FileMeta msgMeta = new FileMeta(meta.getName(), meta.getOwner(), meta.getState(), meta.getVersion(), meta.getTTR(), meta.getLastModified());
				Message m = new Message(this, msgMeta, Type.MessageType.TTRRequest);
				String[] destIpPort = this.peersAddrs.get(meta.getOwner());
				this.sendMessage(m, destIpPort[0], Integer.parseInt(destIpPort[1]));
				System.out.println("[LAZY PULL] Peer " + this.id + " sends TTR request of \"" + meta.getName() + "\" to Peer " + meta.getOwner() + ".");
			}
		}
	}

	/**
	 * A runnable class for server thread
	 */
	protected class StartServer implements Runnable {
		private Peer peer;

		protected StartServer(Peer peer) {
			this.peer = peer;
		}

		@Override
		public void run() {
			peer.openServerSocket();
			System.out.println("Peer " + getId() + " started.");
		}
	}

	/**
	 * Bind the server side to the specific IP and port
	 */
	private void openServerSocket() {
		try {
			this.server = new TCPServer(this);
			System.out.println("Peer " + this.id + " bound to " + this.ip + ":" + String.valueOf(this.port));
			this.server.start();

		} catch (Exception e) {
			System.out.println("Peer " + this.id + " Socket already exists.");
			System.out.println("Peer " + this.id + " Exit.");
			System.exit(0);
		}
	}

	/**
	 * The message processing logic, taking care of 7 types of message, with each proper actions being taken
	 * 
	 * @param m
	 */
	public void processMessage(Message m) {
		synchronized (this) {
			if (this.oldMessages.contains(m)) { // if already saw the message before, discard
				System.out.println("Peer " + this.id + " has seen message{id=" + m.getId()[0] + m.getId()[1] + ", type=" + m.getType() + "} before. Discard.");
				return;
			}
			this.recordMessage(m);
		}

		switch (m.getType()) {
		case Query:
			processQuery(m);
			break;
		case HitQuery:
			processHitQuery(m);
			break;
		case DownloadRequest:
			processDownloadRequest(m);
			break;
		case DownloadReply:
			processDownloadReply(m);
			break;
		case Invalidate:
			processInvalidate(m);
			break;
		case TTRRequest:
			processTTRRequest(m);
			break;
		case TTRReply:
			processTTRReply(m);
			break;
		/* performance evaluation */
		case CompareLastMod:
			FileMeta meta = m.getFileMeta();
			String fileName = meta.getName();
			if (searchFromOwned(fileName)) {
				this.statistics.setQueryCount(this.statistics.getQueryCount() + 1);
				if (meta.getLastModified() < Util.readLocalFiles(this.id, "").get(fileName).lastModified()) {
					this.statistics.setBadQueryCount(this.statistics.getBadQueryCount() + 1);
				}
			}
			break;
		/* performance evaluation */
		}

	}

	/**
	 * Processing logic for receiving a query message.
	 * Search local for the query file. 
	 * If hit, send back the hit query to the original peer.
	 * @param m
	 */
	private void processQuery(Message m) {
		String fileName = m.getFileMeta().getName();
		boolean hasCopy = searchFromCached(fileName);
		if (searchFromOwned(fileName) || hasCopy) {
			/* performance evaluation */
			if (hasCopy) {
				FileMeta copy = this.cachedFiles.get(fileName);
				FileMeta specialMsgMeta = new FileMeta(copy.getName(), copy.getOwner(), copy.getState(), copy.getVersion(), copy.getTTR(), copy.getLastModified());
				Message specialMsg = new Message(this, specialMsgMeta, Type.MessageType.CompareLastMod);
				String[] ownerAddr = this.peersAddrs.get(copy.getOwner());
				this.sendMessage(specialMsg, ownerAddr[0], Integer.parseInt(ownerAddr[1]));
			}
			/* performance evaluation */

			m.setType(Type.MessageType.HitQuery); // change the message type to hit-query
			m.setSenderIpPortId(this); // piggyback this peer info
			if (m.hasUpstreamPeer()) {
				this.relayBackMessage(m);
			}
		} else {
			this.relayForwardMessage(m);
		}
	}

	/**
	 * Process the hit query message.
	 * If not the original peer, relay back the message.
	 * Else get metadata of the hit query and send download request to the owner peer.
	 * @param m
	 */
	private void processHitQuery(Message m) {
		if (m.hasUpstreamPeer()) {
			relayBackMessage(m);
		} else {
			// get sender info
			String srcIp = m.getSenderIpPortId()[0];
			int srcPort = Integer.parseInt(m.getSenderIpPortId()[1]);
			int srcId = Integer.parseInt(m.getSenderIpPortId()[2]);

			m.setType(Type.MessageType.DownloadRequest); // change the message type to download-request
			m.setSenderIpPortId(this); // piggyback this peer info

			this.sendMessage(m, srcIp, srcPort); // send download request to hit-query sender
			System.out.println("Peer " + this.id + " sends dowanload request to Peer " + srcId + ".");
		}
	}

	/**
	 * Process the download request message.
	 * If found the required file, send the download reply back to the original peer.
	 * It first search the owned files, then the cached files.
	 * @param m
	 */
	private void processDownloadRequest(Message m) {
		// get sender info
		String srcIp = m.getSenderIpPortId()[0];
		int srcPort = Integer.parseInt(m.getSenderIpPortId()[1]);
		int srcId = Integer.parseInt(m.getSenderIpPortId()[2]);

		FileMeta msgMeta = m.getFileMeta();
		String fileName = msgMeta.getName();
		File file = null;
		FileMeta originMeta = null;
		if (searchFromOwned(fileName)) {
			file = Util.getFile(fileName, this.id, "");
			originMeta = this.ownFiles.get(fileName);
			if (!file.exists()) { // if owner lost the file
				// send "delete" message to its copies
				// (if requester has the file's copy, it will delete the copy after receiving the message,
				// otherwise requester simply ignores the message)
				msgMeta.setVersion(-1);
				msgMeta.setLastModified(-1);
				System.out.println("Peer " + this.id + "(owner) lost \"" + fileName + "\".");
				m.setSenderIpPortId(this); // piggyback this peer info
				m.setType(Type.MessageType.DownloadReply); // change the message type to download-reply
				this.sendMessage(m, srcIp, srcPort); // send download reply to download request sender
				return;
			} else if (file.exists() && originMeta == null) { // if owner just lost file's meta
				System.out.println("Peer " + this.id + "(owner) lost meta-info of \"" + fileName + "\".");
				return; // maybe try downloading next time
			}
		} else if (searchFromCached(fileName)) {
			file = Util.getFile(fileName, this.id, "c");
			originMeta = this.cachedFiles.get(fileName);
			if (!file.exists() || originMeta == null) { // if file copy lost or its meta lost
				System.out.println("Peer " + this.id + " lost copy of \"" + fileName + "\" or its meta.");
				return; // maybe try downloading next time
			}
		}

		if (originMeta != null && file.exists()) { // if both file and its meta found
			// prepare file content
			try {
				m.setFileContent(Util.fileToBytes(file));
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("Peer " + this.id + " failed to get content of \"" + fileName + "\".");
				return;
			}
			// prepare file meta
			msgMeta.setOwner(originMeta.getOwner());
			msgMeta.setState(originMeta.getState());
			msgMeta.setTTR(originMeta.getTTR());
			msgMeta.setVersion(originMeta.getVersion());
			msgMeta.setLastModified(originMeta.getLastModified());
			System.out.println("Peer " + this.id + " prepares to send \"" + fileName + "\".");
		}

		m.setSenderIpPortId(this); // piggyback this peer info
		m.setType(Type.MessageType.DownloadReply); // change the message type to download-reply
		this.sendMessage(m, srcIp, srcPort); // send download reply to download request sender
		System.out.println("Peer " + this.id + " sends \"" + fileName + "\" to Peer " + srcId + ".");
	}

	/**
	 * Process the download reply message.
	 * If file metadata in reply indicates it's a corrupted file, delete the file locally
	 * else stores the file locally and update the memory
	 * @param m
	 */
	private void processDownloadReply(Message m) {
		/* performance evaluation */
		long msgEndTime = System.nanoTime();
		long msgStartTime = m.getTimeStamp();
		this.statistics.setSumRespTime(this.statistics.getSumRespTime() + (msgEndTime - msgStartTime));
		this.statistics.setValidMsgCount(this.statistics.getValidMsgCount() + 1);
		/* performance evaluation */

		FileMeta msgMeta = m.getFileMeta();
		String fileName = msgMeta.getName();
		// if receiving "delete" message
		if (msgMeta.getVersion() == -1 && msgMeta.getLastModified() == -1) {
			// delete file copy and its meta (nothing will happen if originally no such file)
			this.cachedFiles.remove(fileName);
			Util.deleteMetaFile(fileName, this.id, "c");
			Util.deleteFile(fileName, this.id, "c");
		} else {
			// all download files will be stored in cache folder
			File file = Util.getFile(fileName, this.id, "c");
			// store file content
			try {
				Util.bytesToFile(file, m.getFileContent());
			} catch (IOException e) {
				System.err.println("Writing file content from message failure.");
				e.printStackTrace();
				return;
			}
			// store file meta in memory
			FileMeta meta = new FileMeta(fileName, msgMeta.getOwner(), msgMeta.getState(), msgMeta.getVersion(), msgMeta.getTTR(), msgMeta.getLastModified());
			this.cachedFiles.put(fileName, meta);
			// store file meta on disk
			Util.flushFileMetaToDisk(meta, this.id, "c");
			// if pull-based consistency, schedule setting TTR expired
			if (this.config.getConsistencyMethod().equalsIgnoreCase("pull")) {
				this.scheduler.scheduleSetTTRExpired(this, fileName);
			}
			System.out.println("Peer " + this.id + " received \"" + fileName + "\" successfully.");
		}
	}

	/**
	 * Process invalidate message
	 * If file metadata in message indicates it's a corrupted file, delete the file locally
	 * Else set the file state to be invalid and flushes to disk
	 * @param m
	 */
	private void processInvalidate(Message m) {
		FileMeta origin = m.getFileMeta();
		String fileName = origin.getName();
		if (searchFromCached(fileName)) {
			// if receiving "delete" message
			if (origin.getVersion() == -1 && origin.getLastModified() == -1) {
				// delete file copy and its meta (nothing will happen if originally no such file)
				this.cachedFiles.remove(fileName);
				Util.deleteMetaFile(fileName, this.id, "c");
				Util.deleteFile(fileName, this.id, "c");
			} else {
				FileMeta copy = this.cachedFiles.get(fileName);
				System.out.println("Peer " + this.id + " compares " + copy + " with " + origin);
				if (origin.getVersion() != copy.getVersion() || origin.getLastModified() != copy.getLastModified()) {
					copy.setState(Type.FileType.Invalid);
					Util.flushFileMetaToDisk(copy, this.id, "c");
					System.out.println("[OUT-OF-DATE] Peer " + this.id + ": " + copy + " out of date, set Invalid.");
				}
			}
		}
		this.relayForwardMessage(m);
	}

	/**
	 * Process TTR request message
	 * If found the file, send message with updated information back
	 * Else tell the peer that the file has been deleted.
	 * @param m
	 */
	private void processTTRRequest(Message m) {
		// get sender info
		String srcIp = m.getSenderIpPortId()[0];
		int srcPort = Integer.parseInt(m.getSenderIpPortId()[1]);
		int srcId = Integer.parseInt(m.getSenderIpPortId()[2]);

		FileMeta copy = m.getFileMeta();
		String fileName = copy.getName();
		if (searchFromOwned(fileName)) {
			FileMeta origin = this.ownFiles.get(fileName);
			// send onwer's version back
			copy.setTTR(origin.getTTR());
			copy.setVersion(origin.getVersion());
			copy.setLastModified(origin.getLastModified());
		} else {
			File file = Util.getFile(fileName, this.id, "");
			if (!file.exists()) { // if original file lost
				// send "delete" message
				copy.setVersion(-1);
				copy.setLastModified(-1);
				System.out.println("Peer " + this.id + "(owner) lost \"" + fileName + "\".");
			}
		}

		m.setSenderIpPortId(this); // piggyback this peer info
		m.setType(Type.MessageType.TTRReply); // change the message type to TTR-reply
		this.sendMessage(m, srcIp, srcPort); // send TTR reply to TTR request sender
		System.out.println("Peer " + this.id + " sends TTR reply of " + m.getFileMeta() + " to Peer " + srcId + ".");
	}

	/**
	 * Process the TTR reply message.
	 * If version is not changed, simply set file TTR and schedule a new period
	 * Else if it is a corrupted file, delete the file locally
	 * Else if the version number is different, set the file to be invalid and flushes the meta to disk
	 * @param m
	 */
	private void processTTRReply(Message m) {
		FileMeta origin = m.getFileMeta();
		String fileName = origin.getName();
		FileMeta copy = this.cachedFiles.get(fileName);
		if (origin.getVersion() == copy.getVersion() && origin.getLastModified() == copy.getLastModified()) {
			// no change of file, simply schedule setting TTR expired and set state to be valid
			copy.setTTR(origin.getTTR());
			this.scheduler.scheduleSetTTRExpired(this, fileName);
			copy.setState(Type.FileType.Valid);
			Util.flushFileMetaToDisk(copy, this.id, "c");
			System.out.println("Peer " + this.id + ": " + copy + " is up to date, only TTR renewed.");
		} else if (origin.getVersion() == -1 && origin.getLastModified() == -1) {
			// delete file copy and its meta (nothing will happen if originally no such file)
			this.cachedFiles.remove(fileName);
			Util.deleteMetaFile(fileName, this.id, "c");
			Util.deleteFile(fileName, this.id, "c");
		} else {
			System.out.println("Peer " + this.id + " compares " + copy + " with " + origin);
			copy.setState(Type.FileType.Invalid);
			Util.flushFileMetaToDisk(copy, this.id, "c");
			System.out.println("[OUT-OF-DATE] Peer " + this.id + ": " + copy + " out of date, set Invalid.");
		}
	}

	/**
	 * Search the local owned files in memory
	 * 
	 * @param m
	 * @return boolean if owned files list has the required file
	 */
	private boolean searchFromOwned(String fileName) {
		if (this.ownFiles.containsKey(fileName)) {
			System.out.println("File \"" + fileName + "\" found on Peer " + this.id);
			return true;
		} else
			return false;
	}

	/**
	 * Search cached file in memory
	 * @param fileName
	 * @return
	 */
	private boolean searchFromCached(String fileName) {
		if (this.cachedFiles.containsKey(fileName) && this.cachedFiles.get(fileName).getState() == Type.FileType.Valid) {
			System.out.println("File copy \"" + fileName + "\" found on Peer " + this.id);
			return true;
		} else
			return false;
	}

	/**
	 * Push message into the old message buffer
	 * 
	 * @param m
	 */
	private void recordMessage(Message m) {
		// control the size of old messages
		if (this.oldMessages.size() >= Configuration.MAX_NUM_OLD_MSGS)
			this.oldMessages.poll();
		this.oldMessages.add(m);
	}

	/**
	 * Send message back to the previous peer who sends the message Message will follow the same path back to the origin peer as how it comes from.
	 * 
	 * @param m
	 */
	private void relayBackMessage(Message m) {
		int nextPeer = m.popPeer();
		String[] addr = this.peersAddrs.get(nextPeer);
		String ip = addr[0];
		int port = Integer.parseInt(addr[1]);
		this.sendMessage(m, ip, port);
		System.out.println("Peer " + this.id + " relays back " + m + " to Peer " + nextPeer + ".");
	}

	/**
	 * Send messages to all the neighbors. Before sent, TTL of message will be checked and minus one.
	 * 
	 * @param m
	 */
	public void relayForwardMessage(Message m) {
		// relay forward message to neighbors
		if (m.getTTL() > 0) {
			m.pushPeer(this.id);
			m.minusTTL();
			this.broadcastMessage(m);
		}
	}

	/**
	 * Send message to all its neighbors
	 * 
	 * @param m
	 */
	private void broadcastMessage(Message m) {
		for (int i : this.neighbors) {
			String[] addr = this.peersAddrs.get(i);
			this.sendMessage(m, addr[0], Integer.parseInt(addr[1]));
			System.out.println("Peer " + this.id + " relays forward " + m + " to Peer " + i + ".");
		}
	}

	/**
	 * Send message to the specific destination based on IP and Port
	 * 
	 * @param m
	 * @param destIp
	 * @param destPort
	 */
	public void sendMessage(Message m, String destIp, int destPort) {
		TCPClient client = new TCPClient(destIp, destPort);
		client.sendMessage(m);
	}

	/**
	 * Runnable object for multi-threading download
	 * 
	 */
	protected class RunDownload implements Runnable {
		private String filename;
		private Peer peer;

		protected RunDownload(Peer peer, String filename) {
			this.peer = peer;
			this.filename = filename;
		}

		@Override
		public void run() {
			if (peer.searchFromOwned(filename) || peer.searchFromCached(filename)) {
				System.out.println("Peer " + peer.id + " already has file \"" + this.filename + "\".");
				return;
			}
			try {
				FileMeta meta = new FileMeta(this.filename, -1, Type.FileType.Invalid, -1, -1, -1);
				Message m = new Message(this.peer, meta, Type.MessageType.Query);
				/* performance evaluation */
				long msgStartTime = System.nanoTime();
				m.setTimeStamp(msgStartTime);
				/* performance evaluation */
				this.peer.recordMessage(m);
				this.peer.relayForwardMessage(m);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * It creates a thread for peer to download a file
	 * 
	 * @param filename
	 *            name of file
	 */
	public void spawnDownloadThread(String filename) {
		Thread t = new Thread(new RunDownload(this, filename));
		t.start();
	}

	/**
	 * Main function to start the peer.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		int id = Integer.parseInt(Util.readProperty(Configuration.CONFIG_FILE, "peerId"));
		Peer peer = new Peer(id);
		peer.startService();

		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("Please input the filename to download or " + "input '!' to quit:");

			String input = sc.nextLine();

			if ("!".equals(input)) {
				sc.close();
				System.exit(0);
			}
			peer.spawnDownloadThread(input);
		}
	}
}
