package peer;

import java.util.Timer;
import java.util.TimerTask;
import model.FileMeta;

/**
 * This class implements several schedulers, such as
 * 1. synchronize files between memory and disk every n seconds
 * 2. schedule new download request if file is invalid
 * 3. schedule TTR expiration for file
 * 
 */
public class Scheduler {
	Peer peer;

	public Scheduler(Peer peer) {
		this.peer = peer;
	}

	protected void scheduleSyncFiles(int delayInSec, int periodInSec) {
		Timer t = new Timer();
		t.scheduleAtFixedRate(new SyncFilesTask(this.peer), delayInSec * 1000, periodInSec * 1000);
	}

	private class SyncFilesTask extends TimerTask {
		Peer p;

		private SyncFilesTask(Peer p) {
			super();
			this.p = p;
		}

		@Override
		public void run() {
			this.p.syncFiles();
		}
	}

	protected void scheduleDownloadNewCopies(int delayInSec, int periodInSec) {
		Timer t = new Timer();
		t.scheduleAtFixedRate(new RefreshCacheTask(this.peer), delayInSec * 1000, periodInSec * 1000);
	}

	private class RefreshCacheTask extends TimerTask {
		private Peer p;

		private RefreshCacheTask(Peer p) {
			this.p = p;
		}

		@Override
		public void run() {
			this.p.downloadNewCopies();
		}

	}

	protected void scheduleSetTTRExpired(Peer peer, String fileName) {
		FileMeta meta = peer.getCachedFiles().get(fileName);
		if (meta != null && meta.getTTR() >= 0) {
			Timer t = new Timer();
			long delay = (long) meta.getTTR() * 1000;
			t.schedule(new SetTTRExpiredTask(peer, fileName), delay);
		}
	}

	private class SetTTRExpiredTask extends TimerTask {
		private Peer p;
		private String fileName;

		private SetTTRExpiredTask(Peer p, String fileName) {
			this.p = p;
			this.fileName = fileName;
		}

		@Override
		public void run() {
			this.p.setTTRExpired(fileName);
		}

	}

	protected void scheduleTTRExpiredCheck(int delayInSec, int periodInSec) {
		Timer t = new Timer();
		t.scheduleAtFixedRate(new TTRExpiredCheckTask(this.peer), delayInSec * 1000, periodInSec * 1000);
	}

	private class TTRExpiredCheckTask extends TimerTask {
		private Peer p;

		TTRExpiredCheckTask(Peer p) {
			this.p = p;
		}

		@Override
		public void run() {
			this.p.checkTTRExpired();
		}
	}

}
