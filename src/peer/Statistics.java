package peer;

/**
 * This class is for performance evaluation.
 *
 */
public class Statistics {
	private int validMsgCount = 0;
	private long sumRespTime = 0;
	private int queryCount = 0;
	private int badQueryCount = 0;

	public static final double NANO_FACTOR = 1000000000.0;

	public int getValidMsgCount() {
		return validMsgCount;
	}

	public void setValidMsgCount(int validMsgCount) {
		this.validMsgCount = validMsgCount;
	}

	public long getSumRespTime() {
		return sumRespTime;
	}

	public void setSumRespTime(long sumRespTime) {
		this.sumRespTime = sumRespTime;
	}

	public int getQueryCount() {
		return queryCount;
	}

	public void setQueryCount(int queryCount) {
		this.queryCount = queryCount;
	}

	public int getBadQueryCount() {
		return badQueryCount;
	}

	public void setBadQueryCount(int badQueryCount) {
		this.badQueryCount = badQueryCount;
	}
}
