package peer;

import java.util.Properties;

import util.Util;

/**
 * This class stores all the configuration information read from the config file
 * It will later be used by other class for easy access to configuration parameters.
 *
 */
public class Configuration {
	private int peerId;
	private String peerIp;
	private int basePort;
	private String topology = "star";
	private int TTL = 5;
	private int TTR = 600;
	private int refreshPeriod = 15;
	private String consistencyMethod = "push";
	private int pullPeriod = 10;

	public static final String CONFIG_FILE = "config.properties";
	public static final int MAX_NUM_OLD_MSGS = 1000;
	public static final int FILES_SYNC_PERIOD = 2;

	public Configuration() {
		Properties properties = Util.readAllProperties(CONFIG_FILE);
		this.peerId = Integer.parseInt(properties.getProperty("peerId"));
		this.peerIp = properties.getProperty("ip");
		this.basePort = Integer.parseInt(properties.getProperty("basePort"));
		this.topology = properties.getProperty("topology");
		this.TTL = Integer.parseInt(properties.getProperty("TTL"));
		this.TTR = Integer.parseInt(properties.getProperty("TTR"));
		this.refreshPeriod = Integer.parseInt(properties.getProperty("refreshPeriod"));
		this.consistencyMethod = properties.getProperty("consistencyMethod");
		this.pullPeriod = Integer.parseInt(properties.getProperty("pullPeriod"));
	}

	public int getPeerId() {
		return peerId;
	}

	public void setPeerId(int peerId) {
		this.peerId = peerId;
	}

	public String getPeerIp() {
		return peerIp;
	}

	public void setPeerIp(String peerIp) {
		this.peerIp = peerIp;
	}

	public int getBasePort() {
		return basePort;
	}

	public void setBasePort(int basePort) {
		this.basePort = basePort;
	}

	public String getTopology() {
		return topology;
	}

	public void setTopology(String topology) {
		this.topology = topology;
	}

	public int getTTL() {
		return TTL;
	}

	public void setTTL(int ttl) {
		TTL = ttl;
	}

	public int getTTR() {
		return TTR;
	}

	public void setTTR(int ttr) {
		TTR = ttr;
	}

	public int getRefreshPeriod() {
		return refreshPeriod;
	}

	public void setRefreshPeriod(int refreshPeriod) {
		this.refreshPeriod = refreshPeriod;
	}

	public String getConsistencyMethod() {
		if (!"push".equalsIgnoreCase(this.consistencyMethod) && !"pull".equalsIgnoreCase(this.consistencyMethod))
			return "push";
		else
			return this.consistencyMethod;
	}

	public void setConsistencyMethod(String consistencyMethod) {
		this.consistencyMethod = consistencyMethod;
	}

	public int getPullPeriod() {
		return pullPeriod;
	}

	public void setPullPeriod(int pullPeriod) {
		this.pullPeriod = pullPeriod;
	}
}
